# https://docs.blender.org/api/current/

import bpy, math, mathutils, random, time

DRY_RUN = 			False
ROTATION_RANGES = 	[[0,0],[0,0],[-90,90]]
SCALE_RANGES = 		[[1,3],[1,1],[1,1]]
SCALE_LOCKED = 		True
OUTPUT = 			[128,128]

def randomise_material( mat ):
	principled = mat.node_tree.nodes["Principled BSDF"]
	if principled == None:
		return False
	rcolor = mathutils.Color()
	rcolor.hsv = ( random.random(), 1.0, 1.0 )
	principled.inputs["Base Color"].default_value = ( rcolor.r, rcolor.g, rcolor.b, 1.0 )
	# randomise emission if some
	emission_slot = principled.inputs["Emission"].default_value
	emission_color = mathutils.Color(( emission_slot[0], emission_slot[1], emission_slot[2] ))
	if emission_color != mathutils.Color((0.0,0.0,0.0)):
		rcolor.hsv = ( random.random(), 1.0, 1.0 )
		principled.inputs["Emission"].default_value = ( rcolor.r, rcolor.g, rcolor.b, 1.0 )
	return True

def randomise_light( light ):
	rcolor = mathutils.Color()
	rcolor.hsv = ( random.random(), 1.0, 1.0 )
	light.color = rcolor

def randomise_background( world ):
	rcolor = mathutils.Color()
	rcolor.hsv = ( random.random(), 1.0, 1.0 )
	world.node_tree.nodes["Background"].inputs[0].default_value = ( rcolor.r, rcolor.g, rcolor.b, 1.0 )

for obj in bpy.data.objects:
	#print( obj.type )
	if obj.type == "MESH":
		has_mat = False
		for mat in obj.data.materials:
			if randomise_material( mat ):
				has_mat = True
		if has_mat and obj.parent == None:
			oscale = 1
			if SCALE_LOCKED:
				oscale = SCALE_RANGES[0][0] + (SCALE_RANGES[0][1]-SCALE_RANGES[0][0])*random.random()
			for axis in range( 0,3 ):
				obj.rotation_euler[axis] += ( ROTATION_RANGES[axis][0] + (ROTATION_RANGES[axis][1]-ROTATION_RANGES[axis][0])*random.random() ) / 180 * math.pi
				if SCALE_LOCKED:
					obj.scale[axis] *= oscale
				else:
					obj.scale[axis] *= SCALE_RANGES[axis][0] + (SCALE_RANGES[axis][1]-SCALE_RANGES[axis][0])*random.random()
		obj.data.update()
	elif obj.type == "LIGHT":
		if obj.data.type == "POINT":
			randomise_light( obj.data )

for world in bpy.data.worlds:
	randomise_background( world )

if not DRY_RUN:
	now = int( time.time_ns() / 1000 )
	render = bpy.context.scene.render
	render.resolution_x = OUTPUT[0]
	render.resolution_y = OUTPUT[1]
	render.filepath = '//renders/render_' + str(now)
	render.engine = 'CYCLES'
	bpy.context.view_layer.update()
	bpy.ops.render.render( "EXEC_DEFAULT", animation=False, write_still=True )
