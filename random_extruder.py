import bpy, random, mathutils, copy

obj = bpy.context.active_object

EXTRUSIONS = 10
MOTION_RANGES = [[-0.1,0.1],[-0.1,0.1],[0.5,1.0]]
SCALES = [0.9,1.1]
MOTION_DECAY = 0.99
SCALE_DECAY = 0.9
REPEAT = 5

if obj != None and obj.type == "MESH":

	for ext in range(0, EXTRUSIONS):
		
		# deselect all
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all( action = 'DESELECT' )
		bpy.ops.object.mode_set(mode='OBJECT')
		
		mesh = obj.data
		# select a random face
		pcount = len( mesh.polygons )
		
		pid = int( random.random() * pcount ) % pcount
		mesh.polygons[pid].select = True
		
		normal = mesh.polygons[pid].normal
		vec_norm = mathutils.Vector(normal)
		vec_Z = mathutils.Vector((0.0,0.0,1.0))
		vec_right = vec_norm.cross( vec_Z ).normalized()
		vec_up = vec_right.cross( vec_norm ).normalized()
		
		motion = copy.deepcopy( MOTION_RANGES )
		scales = copy.deepcopy( SCALES )
		
		for r in range( 0, REPEAT ):
			
			translation = mathutils.Vector()
			translation += vec_right * ( motion[0][0] + (motion[0][1]-motion[0][0])*random.random() )
			translation += vec_up * ( motion[1][0] + (motion[1][1]-motion[1][0])*random.random() )
			translation += vec_norm * ( motion[2][0] + (motion[2][1]-motion[2][0])*random.random() )
			sca = scales[0] + (scales[1]-scales[0]) * random.random()
			
			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.mesh.extrude_region_move( TRANSFORM_OT_translate={ "value":translation } )
			bpy.ops.transform.resize( value=(sca,sca,sca),orient_type='LOCAL')
			bpy.ops.object.mode_set(mode='OBJECT')
			
			for mr in motion:
				for i in range(0,2):
					mr[i] *= MOTION_DECAY
			for i in range(0,2):
				scales[i] *= SCALE_DECAY
