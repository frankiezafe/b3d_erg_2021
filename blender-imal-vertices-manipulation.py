import bpy
import random

bpy.ops.mesh.primitive_cube_add()
cube = bpy.context.active_object

bpy.ops.object.editmode_toggle()
bpy.ops.mesh.primitive_cube_add( location=( -5,1,1 ) )
bpy.ops.object.editmode_toggle()

for v in cube.data.vertices:
    print( v.co )
    v.co.x = -2.5 + random.random() * 5

print( cube.data.vertices )
