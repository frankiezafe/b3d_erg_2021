import time, os, bpy

BLENDER_PATH = 		'/opt/blender-3.0.0/./blender'
FILE_PATH = 		os.path.abspath('renderer.blend')
SCRIPT_PATH = 		os.path.abspath('random_render.py')
BACKGROUND = 		True
RENDER = 			False
RUN_COUNT = 		16
DRY_RUN = 			False

for r in range(0,RUN_COUNT):
	# building command
	cmd = BLENDER_PATH + ' ' + FILE_PATH
	if BACKGROUND:
		cmd += ' -b'
	if RENDER:
		# generation of a unique number
		now = int( time.time_ns() / 1000 )
		cmd += ' -o //render_' + str(now) + '_ -F PNG -x 1 -f'
	if SCRIPT_PATH != None:
		cmd += ' --enable-autoexec --python ' + SCRIPT_PATH
	if DRY_RUN:
		print( cmd )
	else:
		os.system( cmd )
