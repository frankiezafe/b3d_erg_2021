import bpy
import math
import random

bpy.ops.object.select_all( action = 'SELECT' )
bpy.ops.object.delete()

bpy.ops.object.empty_add()
root = bpy.context.active_object

gridsize = 2

for z in range( -gridsize, gridsize + 1 ):

    for y in range( -gridsize, gridsize + 1 ):

        for x in range( -gridsize, gridsize + 1 ):

            bpy.ops.mesh.primitive_cube_add()
            cube = bpy.context.active_object
            cube.name = 'c_' + str(x) + '_' + str( y ) + '_' + str( z )
            cube.location.x = x * 2.5
            cube.location.y = y * 2.5
            cube.location.z = z * 2.5
            cube.rotation_euler.x = random.random() * math.pi * 2
            cube.rotation_euler.y = random.random() * math.pi * 2
            cube.rotation_euler.z = random.random() * math.pi * 2
            cube.parent = root
