import bpy, mathutils, random

# VARIABLES

LENGTH_RANGE = [0.1, 0.5]
DEVIATION = 0.05

# FUNCTIONS

def randr( f0, f1 ):
    return f0 + (f1-f0) * random.random()

def branch_curve( curve ):
    
    spline = curve.splines[0]
    knot1 = spline.bezier_points[0].co
    handle1 = spline.bezier_points[0].handle_right
    handle2 = spline.bezier_points[1].handle_left
    knot2 = spline.bezier_points[1].co
    points = mathutils.geometry.interpolate_bezier(knot1, handle1, handle2, knot2, 1000)
    
    pid = random.randint(1,1000)
    previous = pid-1
    
    pA = points[previous]
    pB = points[pid]
    diff = ( pB - pA ).normalized()
    perp = mathutils.Vector((0,0,1)).cross( diff )
    
    #print( diff )
    #print( perp )
    bpy.data.objects["Cube"].location = pB
    bpy.data.objects["Cube2"].location = pB + perp * 0.2
    bpy.ops.curve.primitive_bezier_curve_add(enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
    
    newcurve = bpy.context.active_object
   
    length = randr( LENGTH_RANGE[0], LENGTH_RANGE[1] )
    if random.random() < 0.5:
        length *= -1
    
    deviation = DEVIATION
    
    newcurve.data.splines[0].bezier_points[0].co = pB
    newcurve.data.splines[0].bezier_points[0].handle_left = pB + perp * -length*0.25
    newcurve.data.splines[0].bezier_points[0].handle_right = pB + perp * length*0.25 + diff * randr(-deviation,deviation)
    newcurve.data.splines[0].bezier_points[1].handle_left = pB + perp * length*0.75 + diff * randr(-deviation,deviation)
    newcurve.data.splines[0].bezier_points[1].co = pB + perp * length
    
    return newcurve
    
# MAIN

root = bpy.data.objects["bezier"]
curve = root.data

branch_curve( bpy.data.objects["BezierCurve.002"].data )